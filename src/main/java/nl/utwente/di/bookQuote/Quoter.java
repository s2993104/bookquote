package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> ISBNTable = new HashMap<>();
    public Quoter(){
        this.ISBNTable.put("1", 10.0);
        this.ISBNTable.put("2", 45.0);
        this.ISBNTable.put("3", 20.0);
        this.ISBNTable.put("4", 35.0);
        this.ISBNTable.put("5", 50.0);
    }
    public double getBookPrice(String isbn){
        return ISBNTable.get(isbn);
    }
}
